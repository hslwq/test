from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.


class User(AbstractUser):

    # 在AbstractUser基础上进行扩展，增加自己需要的字段
    # 使用models.CharField来指定字符类型的字段，max_length设定最大长度，unique表示是否唯一，blank表示是否可为空
    phone_number = models.CharField(max_length=11, unique=True, blank=False)
    avatar = models.ImageField(upload_to='upload/avatar/%Y%m%d', blank=True)
    user_desc = models.CharField(max_length=500, blank=True)

    class Meta:
        # 修改表名
        db_table = 'users'
        # admin后台显示
        verbose_name = '用户管理'
        # admin后台显示
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.phone_number

