from django.urls import path

from users.views import RegisterView, ImageCodeView

urlpatterns = [
    # path 的第一个参数：路由
    # path 的第二个参数：视图的函数名
    path('register/', RegisterView.as_view(), name='register'),
    path('imagecode/', ImageCodeView.as_view(), name='imagecode'),
]
