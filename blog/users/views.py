from django.http import HttpResponseBadRequest, HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views import View
from django_redis import get_redis_connection

from libs.captcha.captcha import captcha


class RegisterView(View):

    def get(self, request):
        return render(request, 'register.html')


class ImageCodeView(View):

    def get(self, request):
        # 1. 接收前端传递的uuid
        uuid = request.GET.get('uuid')

        # 2. 判断uuid是否为空
        if uuid is None:
            return HttpResponseBadRequest('验证码参数获取失败')

        # 3. 通过调用captcha来生成图片验证码（图片二进制和图片内容text）
        imgText, image = captcha.generate_captcha()

        # 4. 将图片内容保存至redis中，uuid作为key（保证唯一性），验证码内容作为value，再给加上一个时效
        redis_conn = get_redis_connection('default')
        # 向redis中添加数据，参数：key, seconds, value
        # key：设置为唯一的uuid，'img:%s'%uuid，是给uuid加上一个前缀
        # seconds：时效（单位：秒）
        # value：验证码内容text
        redis_conn.setex('img:%s' % uuid, 5 * 60, imgText)

        # 返回图片数据（二进制），返回时需要告知浏览器内容为image
        return HttpResponse(image, content_type='image/jpeg')